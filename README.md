# Installation

1. Install a SciPy distribution of Python 2.7. You can find convenient installers [here](http://www.scipy.org/install.html).
2. Download the latest [OpenCV release](http://sourceforge.net/projects/opencvlibrary/files/) and extract it.
3. Go to the `opencv/build/python/2.7` folder and copy cv2.pyd to `C:/Python27/lib/site-packages`.
4. Install [python-tesseract](https://code.google.com/p/python-tesseract/).

# Use
In Python interpreter:

    > execfile('scripts/recognize.py')
    > execfile('scripts/frame.py')
    > compare()
